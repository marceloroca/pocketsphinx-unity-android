﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class SpeechRecognizerDemo : MonoBehaviour, IPocketSphinxEvents
{
    private const String DIGITS_SEARCH = "digits";

    #region Public serialized fields
    [SerializeField]
    private GameObject _pocketSphinxPrefab;
    [SerializeField]
    private Text _infoText;
    [SerializeField]
    private Text _SpeechResult;
    #endregion

    #region Private fields
    private UnityPocketSphinx.PocketSphinx _pocketSphinx;

    private Dictionary<string, string> infoTextDict;
    #endregion

    #region Private methods
    private void SubscribeToPocketSphinxEvents()
    {
        EM_UnityPocketsphinx em = _pocketSphinx.EventManager;

        em.OnBeginningOfSpeech += OnBeginningOfSpeech;
        em.OnEndOfSpeech += OnEndOfSpeech;
        em.OnError += OnError;
        em.OnInitializeFailed += OnInitializeFailed;
        em.OnInitializeSuccess += OnInitializeSuccess;
        em.OnPartialResult += OnPartialResult;
        em.OnPocketSphinxError += OnPocketSphinxError;
        em.OnResult += OnResult;
        em.OnTimeout += OnTimeout;
    }

    private void UnsubscribeFromPocketSphinxEvents()
    {
        EM_UnityPocketsphinx em = _pocketSphinx.EventManager;

        em.OnBeginningOfSpeech -= OnBeginningOfSpeech;
        em.OnEndOfSpeech -= OnEndOfSpeech;
        em.OnError -= OnError;
        em.OnInitializeFailed -= OnInitializeFailed;
        em.OnInitializeSuccess -= OnInitializeSuccess;
        em.OnPartialResult -= OnPartialResult;
        em.OnPocketSphinxError -= OnPocketSphinxError;
        em.OnResult -= OnResult;
        em.OnTimeout -= OnTimeout;
    }

    private void switchSearch()
    {
        _pocketSphinx.StopRecognizer();
        _pocketSphinx.StartListening(DIGITS_SEARCH, 10000);
    }
    #endregion

    #region MonoBehaviour methods
    void Awake()
    {
        UnityEngine.Assertions.Assert.IsNotNull(_pocketSphinxPrefab, "No PocketSphinx prefab assigned.");
        var obj = Instantiate(_pocketSphinxPrefab, this.transform) as GameObject;
        _pocketSphinx = obj.GetComponent<UnityPocketSphinx.PocketSphinx>();

        if (_pocketSphinx == null)
        {
            Debug.LogError("[SpeechRecognizerDemo] No PocketSphinx component found. Did you assign the right prefab???");
        }

        SubscribeToPocketSphinxEvents();

        _infoText.text = "Please wait for Speech Recognition engine to load.";
        _SpeechResult.text = "Loading human dictionary...";
    }

    //private const string LANGUAGE_FOLDER = "en-us";
    //private const string LANGUAGE_FOLDER = "es-es";
    private const string LANGUAGE_FOLDER = "ay-bo";

    void Start()
    {
        _pocketSphinx.SetAcousticModelPath(LANGUAGE_FOLDER + "/ptm");
        _pocketSphinx.SetDictionaryPath(LANGUAGE_FOLDER + "/dictionary.dict");
        _pocketSphinx.SetKeywordThreshold(1e-45f);
        _pocketSphinx.AddBoolean("-allphone_ci", true);

        // These one are optional
        _pocketSphinx.AddGrammarSearchPath(DIGITS_SEARCH, LANGUAGE_FOLDER + "/grammar/digits.gram");

        _pocketSphinx.SetupRecognizer();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnDestroy()
    {
        if (_pocketSphinx != null)
        {
            UnsubscribeFromPocketSphinxEvents();
            _pocketSphinx.DestroyRecognizer();
        }
    }
    #endregion

    #region PocketSphinx event methods
    public void OnPartialResult(string hypothesis)
    {
        _SpeechResult.text = hypothesis;
        switchSearch();
    }

    public void OnResult(string hypothesis)
    {
        _SpeechResult.text = hypothesis;
    }

    public void OnBeginningOfSpeech()
    {
    }

    public void OnEndOfSpeech()
    {
        switchSearch();
    }

    public void OnError(string error)
    {
        Debug.LogError("[SpeechRecognizerDemo] An error ocurred at OnError()");
        Debug.LogError("[SpeechRecognizerDemo] error = " + error);
    }

    public void OnTimeout()
    {
        Debug.Log("[SpeechRecognizerDemo] Speech Recognition timed out");
        switchSearch();
    }

    public void OnInitializeSuccess()
    {
        switchSearch();
        _infoText.text = LANGUAGE_FOLDER;
    }

    public void OnInitializeFailed(string error)
    {
        Debug.LogError("[SpeechRecognizerDemo] An error ocurred on Initialization PocketSphinx.");
        Debug.LogError("[SpeechRecognizerDemo] error = " + error);
    }

    public void OnPocketSphinxError(string error)
    {
        Debug.LogError("[SpeechRecognizerDemo] An error ocurred on OnPocketSphinxError().");
        Debug.LogError("[SpeechRecognizerDemo] error = " + error);
    }
    #endregion
}
